Para compilar el programa, basta con ejecutar el comando make en la terminal
en el directorio donde se encuentra este archivo. El programa estará en el
directorio bin, en el cual se podrá ejecutar sin problemas.

Nota: El programa asume que los nodos se enumeran desde el 1 hasta el número
de nodos máximo leídos por stdin (e.g. del 1 al 6 si se explicitan 6 nodos), y
que su representación está dada por su número a la hora de añadir vértices.
