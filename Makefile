SRCDIR = ./src/
SRC = $(addprefix $(SRCDIR), main.cpp)
CC = g++
CFLAGS = -Wall -g
BINDIR = ./bin/

all: $(SRC)
	$(CC) $(SRC) $(CFLAGS) -o $(BINDIR)output
