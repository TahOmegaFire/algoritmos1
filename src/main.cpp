#include "node.h"
#include <cstdio>
#include <iostream>
#include <sstream>
#include <algorithm>

bool checkPartition(std::vector<node*>, std::pair<int, int>);

int main(int argc, char** argv)
{
	int i;
	std::vector<node*> nodes;
	std::vector<std::pair<int, int>> edges;
	std::vector<std::pair<int, int>> rEdges;
	while(scanf("%d", &i) != EOF)
	{
		nodes.clear();
		edges.clear();
		rEdges.clear();
		for(int j = 0; j < i; ++j)
		{
			nodes.push_back(new node(j));
			//std::cout << "Creating node " << nodes[j]->value << " in " << j << std::endl;
		}
		for(int j = 0; j < i + 1; ++j)
		{
			std::string input;
			std::getline(std::cin, input);
			std::stringstream ss(input);
			int nnode, mnode;
			ss >> nnode;
			while(ss >> mnode)
			{
				nodes[nnode - 1]->addNode(nodes[mnode - 1]);
				//std::cout << "Adding node " << mnode - 1 << " to node " << nnode - 1 << " | current length: " << nodes[nnode - 1]->edges.size() << std::endl;
				edges.push_back(std::pair<int, int>(nnode - 1, mnode - 1));
			}
		}

		for(std::vector<node*>::iterator j = nodes.begin(); j != nodes.end(); ++j)
		{
			if((*j)->edges.empty())
			{
				nodes.erase(j);
				--j;
			}
		}

		for(size_t j = 0; j < edges.size(); ++j)
		{
			//std::cout << "Edge to test: " << edges[j].first << " | " << edges[j].second << std::endl;
			if(checkPartition(nodes, edges[j]))
			{
				if(std::find(rEdges.begin(), rEdges.end(), std::make_pair(edges[j].second, edges[j].first)) == rEdges.end())//iter == rEdges.end())
					rEdges.push_back(edges[j]);
			}
		}

		if(rEdges.size() != 0)
		{
			for(size_t j = 0; j < rEdges.size(); ++j)
				std::cout << rEdges[j].first + 1 << " | " << rEdges[j].second + 1 << std::endl;
		}

		else
			std::cout << "No existe corte\n";
		std::cout << "________________\n";
	}
}

void unpass(std::vector<node*> nodes)
{
	for(size_t i = 0; i < nodes.size(); ++i)
		nodes[i]->pass = false;
}

void bfs(node* a_node, std::pair<int, int> skip)
{
	if(a_node->pass == true)
		return;
	//std::cout << "Passing by node " << a_node->value << std::endl;
	a_node->pass = true;
	if(a_node->edges.size() == 0)
		return;

	for(size_t i = 0; i < a_node->edges.size(); ++i)
	{
		//std::cout << "Going to node " << a_node->edges[i]->value << std::endl;
		if(!(skip.first == a_node->value && skip.second == a_node->edges[i]->value) && !(skip.first == a_node->edges[i]->value && skip.second == a_node->value) )
			bfs(a_node->edges[i], skip);
	}
}

bool checkPartition(std::vector<node*> nodes, std::pair<int, int> skip)
{
	unpass(nodes);
	bfs(nodes[0], skip);
	
	for(size_t i = 0; i < nodes.size(); ++i)
		if(nodes[i]->pass == false) return true;
	return false;
}
