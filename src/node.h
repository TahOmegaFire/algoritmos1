#ifndef NODE_H
#define NODE_H

#include <vector>

class node
{
public:		
	node();
	node(int i) { this->value = i; }
	~node();

	void addNode(node* n_node) { this->edges.push_back(n_node); }

	bool pass;

	int value;

	std::vector<node*> edges;
};

#endif //NODE_H
